﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ChatClient
{
    [Serializable]
    public class Message
    {
        public string Text;
        public DateTime RegDate;

        [OptionalField]
        public int error;
        [OptionalField]
        public string errorText;

        public Message(string msg)
        {
            Text = msg;
            RegDate = DateTime.Now;
        }
    }    
}
