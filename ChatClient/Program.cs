﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.IO;
using System.Threading;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Newtonsoft.Json;

namespace ChatClient
{   
    class Program
    {
        static string userName;
        static string ConfFile = "settings.conf";
        static TcpClient client;
        static NetworkStream stream;

        static void Main(string[] args)
        {
            Settings settings;

            using (var fs = new FileStream(ConfFile, FileMode.OpenOrCreate))
            {
                byte[] array = new byte[fs.Length];
                fs.Read(array, 0, array.Length);
                settings = JsonConvert.DeserializeObject<Settings>(Encoding.Default.GetString(array));
                Console.WriteLine("Настройки загружены");                
            }


            client = new TcpClient();
            try
            {                
                client.Connect(settings.Host, settings.Port); 
                Console.WriteLine(client.Connected ? "Клиент запущен" : "Ошибка подключения к серверу");

                stream = client.GetStream();

                Console.Write("Введите свое имя: ");
                userName = Console.ReadLine();

                var msg = new Message(userName);                
                var formatter = new BinaryFormatter();
                formatter.Serialize(stream, msg);

                Thread receiveThread = new Thread(new ThreadStart(ReceiveMessage));
                receiveThread.Start();
                Console.WriteLine($"Добро пожаловать, {userName}");
                SendMessage();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Disconnect();
            }
            Console.ReadKey();
        }

        static void SendMessage()
        {
            Console.WriteLine("Введите сообщение: ");
            while (true)
            {
                string message = Console.ReadLine();
                var msg = new Message(message);

                var formatter = new BinaryFormatter();
                formatter.Serialize(stream, msg);
            }
        }


        static void ReceiveMessage()
        {
            while (true)
            {
                try
                {
                    byte[] data = new byte[64];
                    StringBuilder builder = new StringBuilder();

                    var formatter = new BinaryFormatter();
                    do
                    {
                        var msg = (Message)formatter.Deserialize(stream);
                        builder.Append(msg.Text);
                    }
                    while (stream.DataAvailable);
                    string message = builder.ToString();
                    Console.WriteLine(message);
                }
                catch
                {
                    Console.WriteLine("Подключение прервано!");
                    Console.ReadLine();
                    Disconnect();
                }
            }
        }

        static void Disconnect()
        {
            stream?.Close();
            client?.Close();
        }
    }
}
