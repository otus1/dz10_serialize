﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace dz10_serialize
{
    [Serializable]
    public class MessageServer
    {
        public string Text;
        public DateTime RegDate;

        [OptionalField]
        public int error;
        [OptionalField]
        public string errorText;

        public MessageServer(string msg)
        {
            Text = msg;
            RegDate = DateTime.Now;
        }
    }    
}
