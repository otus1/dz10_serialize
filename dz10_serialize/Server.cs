﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Runtime.Serialization.Formatters.Binary;

namespace dz10_serialize
{
    public class Server
    {
        private int _port;
        static TcpListener tcpListener;
        List<Client> clients = new List<Client>();

        public Server(int port)
        {
            _port = port;
        }

        protected internal void AddConnection(Client client)
        {
            clients.Add(client);
            Console.WriteLine("Добавлен клиент: " + client.Id);
        }

        protected internal void RemoveConnection(string Id)
        {
            Client client = clients.FirstOrDefault(x => x.Id == Id);
            clients?.Remove(client);
        }

        protected internal void Listen()
        {
            try
            {
                tcpListener = new TcpListener(IPAddress.Any, _port);
                tcpListener.Start();
                Console.WriteLine("=== Сервер запущен ===");
                while (true)
                {
                    TcpClient tcpClient = tcpListener.AcceptTcpClient();
                    Client client = new Client(tcpClient, this);
                    Thread clientThread = new Thread(new ThreadStart(client.Process));
                    clientThread.Start();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                Disconect();
            }
        }
        
        protected internal void SendAllClients(MessageServer msg, string curId)
        {
            var formatter = new BinaryFormatter();

            for (int i = 0; i < clients.Count; i++)
            {
                if (clients[i].Id != curId)
                {
                    formatter.Serialize(clients[i].Stream, msg);
                }
            }
        }


        protected internal void Disconect()
        {
            tcpListener.Stop();
            for (int i = 0; i < clients.Count; i++)
            {
                clients[i].Close();
            }
            Environment.Exit(0);
        }
    }
}
