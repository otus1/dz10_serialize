﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dz10_serialize
{
    public class Settings
    { 
        public string Host { get; set; }
        public int Port { get; set; }
    }
}
