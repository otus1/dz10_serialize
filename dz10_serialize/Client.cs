﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using ChatClient;

namespace dz10_serialize
{
    public class Client
    {
        protected internal string Id { get; private set; }
        protected internal NetworkStream Stream { get; private set; }
        string userName;
        TcpClient client;
        Server server;

        public Client(TcpClient _client, Server _server)
        {
            Id = Guid.NewGuid().ToString();
            client = _client;
            server = _server;
            server.AddConnection(this);

        }
        private void SendMsg(string message)
        {
            server.SendAllClients(new MessageServer(message), this.Id);
            Console.WriteLine(message);
        }

        public void Process()
        {
            
            try
            {
                Stream = client.GetStream();
                string message = GetMessage();
                userName = message;
                SendMsg($"=== Вошел в чат: {message}");

                while (true)
                {
                    try
                    {
                        message = GetMessage();
                        message = $"{userName}: {message}";
                        SendMsg(message);
                    }
                    catch
                    {
                        message = $"{userName}: покинул чат";
                        SendMsg(message);
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                server.RemoveConnection(this.Id);
                Close();
            }
        }
        private string GetMessage()
        {
            var formatter = new BinaryFormatter();
            Message msg = null;

            
            try
            {
                do
                {
                    msg = (Message)formatter.Deserialize(Stream);                    
                }
                while (Stream.DataAvailable); 
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: "+ex.Message);
            }
            

            return msg.Text;
        }
        protected internal void Close()
        {
            Stream?.Close();
            client?.Close();
        }
    }
}
