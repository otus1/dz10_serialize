﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using Newtonsoft.Json;

namespace dz10_serialize
{
    class Program
    {
        static Server server;
        static Thread listenThread;
        static string ConfFile = "settings.conf";

        static void Main(string[] args)
        {

            Settings settings;

            using (var fs = new FileStream(ConfFile, FileMode.OpenOrCreate))
            {
                byte[] array = new byte[fs.Length];
                fs.Read(array, 0, array.Length);
                settings = JsonConvert.DeserializeObject<Settings>(Encoding.Default.GetString(array));
                
                Console.WriteLine("Настройки загружены");
            }

            
            try
            {
                server = new Server(settings.Port);
                listenThread = new Thread(new ThreadStart(server.Listen));
                listenThread.Start();
            }
            catch (Exception ex)
            {
                server.Disconect();
                Console.WriteLine(ex.Message);
            }
        }
    }
}
